package com.pichincha.bank.compoment.client;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pichincha.bank.component.client.io.web.ClientController;
import com.pichincha.bank.component.client.io.web.model.ClientCreateRequest;
import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.client.service.ClientService;
import com.pichincha.bank.component.client.service.ClientService.ClientCmd;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@WebMvcTest(ClientController.class)
public class ClientControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ClientService clientService;

  private static final Long CLIENT_ID = 1L;

  @Test
  public void testCreate() throws Exception {

    ClientCreateRequest request = new ClientCreateRequest();
    request.setName("John Doe");
    request.setAddress("Country 154");
    request.setPhone("544545");
    request.setPassword("122");
    request.setIdentification("545445");

    Client client = new Client();
    client.setId(1L);
    client.setName("John Doe");
    client.setAddress("Country 154");


    when(clientService.create(any(ClientCmd.class))).thenReturn(client);

    mockMvc.perform(post("/api/v1/clients")
            .contentType(MediaType.APPLICATION_JSON)
            .content(new ObjectMapper().writeValueAsString(request)))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.name").value("John Doe"))
        .andExpect(jsonPath("$.address").value("Country 154"));
  }

  @Test
  public void deleteClient_shouldReturnNoContent() throws Exception {
    Long clientId = 1L;

    mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/clients/{id}", clientId)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isNoContent());
  }

  @Test
  public void deleteClient_shouldThrowResourceNotFoundException() throws Exception {
    Long clientId = 1L;

    Mockito.doThrow(ResourceNotFoundException.class)
        .when(clientService).delete(clientId);

    mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/clients/{id}", clientId)
            .contentType(MediaType.APPLICATION_JSON))
        .andExpect(MockMvcResultMatchers.status().isNotFound());
  }

  @Test
  public void testUpdateClient() throws Exception {

    ClientCreateRequest request = new ClientCreateRequest();
    request.setName("John Doe");
    request.setAddress("123 Main St");
    request.setPhone("555-1234");
    request.setPassword("122");
    request.setIdentification("545445");
    ClientCmd command = ClientCreateRequest.toModel(request);
    Client client = new Client();
    client.setId(CLIENT_ID);
    client.setName(command.getName());
    client.setAddress(command.getAddress());
    client.setPhone(command.getPhone());

    when(clientService.update(eq(CLIENT_ID), any(ClientCmd.class))).thenReturn(client);

    mockMvc.perform(put("/api/v1/clients/{id}", CLIENT_ID)
            .contentType(MediaType.APPLICATION_JSON)
            .content(new ObjectMapper().writeValueAsString(request)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name").value("John Doe"))
        .andExpect(jsonPath("$.address").value("123 Main St"));
  }
}

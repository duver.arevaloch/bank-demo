CREATE TABLE user (
    id BIGINT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    gender VARCHAR(20) NULL,
    age INT NOT NULL,
    identification VARCHAR(20) NOT NULL,
    address VARCHAR(255)  NULL,
    phone VARCHAR(20)  NULL,
    PRIMARY KEY (id)
);

CREATE TABLE client (
  id BIGINT NOT NULL,
  password VARCHAR(255) NOT NULL,
  state BIT(1) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES user(id)
);

CREATE TABLE account (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  number VARCHAR(20) NOT NULL,
  type VARCHAR(20) NOT NULL,
  balance_initial DOUBLE NOT NULL,
  state BIT(1) NOT NULL,
  client_id BIGINT,
  FOREIGN KEY (client_id) REFERENCES client (id)
);

CREATE TABLE movements (
  id BIGINT AUTO_INCREMENT PRIMARY KEY,
  date DATE NOT NULL,
  type VARCHAR(20) NOT NULL,
  value DOUBLE NOT NULL,
  balance DOUBLE NOT NULL,
  account_id BIGINT,
  FOREIGN KEY (account_id) REFERENCES account (id)
);
package com.pichincha.bank.component.movement.io.web.model;

import com.pichincha.bank.component.movement.model.Movement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovementCreateResponse implements Serializable {

  private static final long serialVersionUID = 1491390367447850683L;

  private String accountNumber;

  private String accountType;

  private Double balance;

  private Boolean state;

  private String detail;

  public static MovementCreateResponse fromModel(Movement movement) {
    return MovementCreateResponse.builder()
        .accountNumber(movement.getAccount().getNumber())
        .accountType(movement.getAccount().getType().getType())
        .balance(movement.getBalance())
        .state(movement.getAccount().isState())
        .detail("")
        .build();

  }
}

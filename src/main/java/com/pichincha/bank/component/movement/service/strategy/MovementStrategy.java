package com.pichincha.bank.component.movement.service.strategy;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.service.MovementService.MovementCmd;
import lombok.NonNull;

public interface MovementStrategy {

  Movement createMovement(@NonNull MovementCmd movementCmd, @NonNull Account account);
}

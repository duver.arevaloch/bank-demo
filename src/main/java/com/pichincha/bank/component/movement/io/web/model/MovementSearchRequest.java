package com.pichincha.bank.component.movement.io.web.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovementSearchRequest implements Serializable {
  private static final long serialVersionUID = 1491390327447850683L;

  @NotNull
  private Date fromDate;

  @NotNull
  private Date toDate;

  @NotNull
  private Long clientId;
}

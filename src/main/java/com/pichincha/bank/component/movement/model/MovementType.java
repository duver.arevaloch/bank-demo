package com.pichincha.bank.component.movement.model;

import com.pichincha.bank.component.shared.web.exception.BadRequestException;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Getter
public enum MovementType {
  DEBIT("debit"),
  CREDIT("credit");

  private final String type;

  private static final String INVALID_TYPE_MESSAGE = "Invalid type value ['%s'] - [Allowed values: %s]";

  private static final String VALUES_JOINER = "', '";

  public static MovementType fromType(String type) {

    try {
      return valueOf(type.toUpperCase());
    } catch (IllegalArgumentException exception) {
      throw new BadRequestException(String.format(INVALID_TYPE_MESSAGE, type, getListOfTypesAsString()));
    }
  }

  public static String getListOfTypesAsString() {
    return String.format("'%s'", Stream.of(values()).map(MovementType::name).collect(Collectors.joining(VALUES_JOINER)));
  }
}

package com.pichincha.bank.component.movement.io.gateway;

import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchRequest;
import com.pichincha.bank.component.movement.model.Movement;
import lombok.NonNull;
import java.util.List;

public interface MovementGateway {

  Movement create(@NonNull Movement movement);

  List<Movement> findByPeriod(MovementSearchRequest request, Client client);
}

package com.pichincha.bank.component.movement.service.strategy;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.movement.io.gateway.MovementGateway;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.service.MovementService.MovementCmd;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CreditMovementStrategy implements MovementStrategy {

  @Autowired
  private MovementGateway movementGateway;

  @Override
  public Movement createMovement(@NonNull MovementCmd movementCmd, @NonNull Account account) {

    Movement movementToCreate = MovementCmd.toModel(movementCmd);

    account.setBalanceInitial(movementCmd.getValue() + account.getBalanceInitial());

    movementToCreate.setBalance(account.getBalanceInitial());
    movementToCreate.setAccount(account);

    return movementGateway.create(movementToCreate);
  }
}

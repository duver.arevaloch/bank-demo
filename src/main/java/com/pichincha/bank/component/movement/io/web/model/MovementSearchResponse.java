package com.pichincha.bank.component.movement.io.web.model;

import com.pichincha.bank.component.account.model.AccountType;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.model.MovementType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import java.io.Serializable;
import java.util.Date;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovementSearchResponse implements Serializable {
  private static final long serialVersionUID = 1491390367447850683L;

  private Date date;

  private String clientName;

  private String accountNumber;

  private AccountType type;

  private double balanceInitial;

  private boolean state;

  private double value;

  private double balanceAvailable;

  public static MovementSearchResponse fromModel(Movement movement) {
    MovementSearchResponse response = MovementSearchResponse.builder()
        .date(movement.getDate())
        .clientName(movement.getAccount().getClient().getName())
        .accountNumber(movement.getAccount().getNumber())
        .type(movement.getAccount().getType())
        .state(movement.getAccount().isState())
        .value(movement.getValue())
        .balanceAvailable(movement.getBalance())
        .build();

    if (MovementType.CREDIT.equals(movement.getType())) {
      response.setBalanceInitial(movement.getBalance() - movement.getValue());
    } else {
      response.setBalanceInitial(movement.getBalance() + movement.getValue());
    }

    return response;
  }
}

package com.pichincha.bank.component.movement.service.strategy;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.movement.io.gateway.MovementGateway;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.service.MovementService.MovementCmd;
import com.pichincha.bank.component.shared.web.exception.BadRequestException;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DebitMovementStrategy implements MovementStrategy {
  private static final String UNAVAILABLE_BALANCE_EXCEPTION_MESSAGE = "Unavailable balance.";

  @Autowired
  private MovementGateway movementGateway;

  @Override
  public Movement createMovement(@NonNull MovementCmd movementCmd, Account account) {
    Movement movementToCreate = MovementCmd.toModel(movementCmd);

    double newBalance = account.getBalanceInitial() - movementCmd.getValue();

    if (newBalance < 0) {
      throw new BadRequestException(UNAVAILABLE_BALANCE_EXCEPTION_MESSAGE);
    }

    account.setBalanceInitial(newBalance);

    movementToCreate.setBalance(account.getBalanceInitial());
    movementToCreate.setAccount(account);

    return movementGateway.create(movementToCreate);
  }
}

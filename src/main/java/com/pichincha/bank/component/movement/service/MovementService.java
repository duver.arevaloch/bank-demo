package com.pichincha.bank.component.movement.service;

import com.pichincha.bank.component.movement.io.web.model.MovementSearchRequest;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchResponse;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.model.MovementType;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import java.util.Date;
import java.util.List;

public interface MovementService {

  Movement create(MovementCmd movementCmd) throws ResourceNotFoundException;

  List<MovementSearchResponse> findByPeriod(MovementSearchRequest request) throws ResourceNotFoundException;

  @Data
  @Generated
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  class MovementCmd {

    private Date date;

    private MovementType type;

    private double value;

    private Long accountId;

    public static Movement toModel(MovementCmd movementCmd) {
      return Movement.builder()
          .date(movementCmd.getDate())
          .type(movementCmd.getType())
          .value(movementCmd.getValue())
          .build();
    }
  }
}

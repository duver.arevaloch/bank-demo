package com.pichincha.bank.component.movement.io.repository;

import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.movement.model.Movement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface MovementRepository extends JpaRepository<Movement, Long> {

  List<Movement> findByDateBetweenAndAccountClient(Date startDate, Date endDate, Client client);
}

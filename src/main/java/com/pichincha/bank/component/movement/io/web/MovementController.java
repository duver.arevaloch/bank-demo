package com.pichincha.bank.component.movement.io.web;

import com.pichincha.bank.component.movement.io.web.model.MovementCreateRequest;
import com.pichincha.bank.component.movement.io.web.model.MovementCreateResponse;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchRequest;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchResponse;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.service.MovementService;
import com.pichincha.bank.component.movement.service.MovementService.MovementCmd;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(path = "/api/v1/movements",
    produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(tags = { "Movements" }, value = "Movements")
@Slf4j
public class MovementController {

  @Autowired
  private MovementService movementService;

  @PostMapping
  @ApiOperation(value = "Create an movement.")
  @ApiResponses(value = { @ApiResponse(code = 201, message = "Created.") })
  @ResponseStatus(value = HttpStatus.CREATED)
  public ResponseEntity<MovementCreateResponse> create(@Valid @NotNull @RequestBody MovementCreateRequest request)
      throws ResourceNotFoundException {
    log.debug("Begin create: request = {}", request);

    MovementCmd command = MovementCreateRequest.toModel(request);

    Movement movementCreated = movementService.create(command);

    log.debug("End create: movementCreated = {}", movementCreated);

    return ResponseEntity.ok(MovementCreateResponse.fromModel(movementCreated));
  }

  @GetMapping("/reports")
  @ApiOperation(value = "Get a list movements by User.")
  @ApiResponses(value = { @ApiResponse(code = 200, message = "Return balance by User.") })
  @ResponseStatus(value = HttpStatus.ACCEPTED)
  public ResponseEntity<List<MovementSearchResponse>> findByPeriod(
      @NotNull @RequestParam(name = "clientId") Long clientId,
      @NotNull @RequestParam(name = "fromDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date fromDate,
      @NotNull @RequestParam(name = "toDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date toDate)
      throws ResourceNotFoundException {
    log.debug("Begin findByPeriod: clientId = {}, fromDate = {}, toDate = {},", clientId, fromDate, toDate);

    MovementSearchRequest request = MovementSearchRequest.builder().clientId(clientId).fromDate(fromDate).toDate(toDate)
        .build();

    List<MovementSearchResponse> movements = movementService.findByPeriod(request);

    log.debug("End findByPeriod: movements = {}", movements);

    return ResponseEntity.ok(movements);
  }
}


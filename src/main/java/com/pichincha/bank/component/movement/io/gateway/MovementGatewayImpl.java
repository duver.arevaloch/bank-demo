package com.pichincha.bank.component.movement.io.gateway;

import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.movement.io.repository.MovementRepository;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchRequest;
import com.pichincha.bank.component.movement.model.Movement;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
@Slf4j
public class MovementGatewayImpl implements MovementGateway {

  @Autowired
  private MovementRepository movementRepository;

  @Override
  public Movement create(@NonNull Movement movement) {
    return movementRepository.save(movement);
  }

  @Override
  public List<Movement> findByPeriod(MovementSearchRequest request, Client client) {
    return movementRepository.findByDateBetweenAndAccountClient(request.getFromDate(), request.getToDate(), client);
  }
}

package com.pichincha.bank.component.movement.service;

import com.pichincha.bank.component.account.io.gateway.AccountGateway;
import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.client.io.gateway.ClientGateway;
import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.movement.io.gateway.MovementGateway;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchRequest;
import com.pichincha.bank.component.movement.io.web.model.MovementSearchResponse;
import com.pichincha.bank.component.movement.model.Movement;
import com.pichincha.bank.component.movement.model.MovementType;
import com.pichincha.bank.component.movement.service.strategy.CreditMovementStrategy;
import com.pichincha.bank.component.movement.service.strategy.DebitMovementStrategy;
import com.pichincha.bank.component.movement.service.strategy.MovementStrategy;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class MovementServiceImpl implements MovementService {

  private static final String ACCOUNT_NOT_FOUND_EXCEPTION_MESSAGE = "Account not found.";

  private static final String CLIENT_NOT_FOUND_EXCEPTION_MESSAGE = "Client not found.";

  @Autowired
  private MovementGateway movementGateway;

  @Autowired
  private CreditMovementStrategy creditMovementStrategy;

  @Autowired
  private DebitMovementStrategy debitMovementStrategy;

  @Autowired
  private AccountGateway accountGateway;

  @Autowired
  private ClientGateway clientGateway;


  @Override
  @Transactional
  public Movement create(MovementCmd movementCmd) throws ResourceNotFoundException {
    log.debug("Begin create: movementCmd = {}", movementCmd);

    Account account = accountGateway.findById(movementCmd.getAccountId())
        .orElseThrow(() -> new ResourceNotFoundException(ACCOUNT_NOT_FOUND_EXCEPTION_MESSAGE));

    MovementStrategy accountStrategy = validateMomentTypeStrategy(movementCmd);

    Movement movementCreated = accountStrategy.createMovement(movementCmd, account);

    log.debug("End create: movementCreated = {}", movementCreated);

    return movementCreated;
  }

  @Override
  @Transactional(readOnly = true)
  public List<MovementSearchResponse> findByPeriod(MovementSearchRequest request) throws ResourceNotFoundException {
    log.debug("Begin create: request = {}", request);

    Client client = clientGateway.findById(request.getClientId())
        .orElseThrow(() -> new ResourceNotFoundException(CLIENT_NOT_FOUND_EXCEPTION_MESSAGE));

    List<Movement> movements = movementGateway.findByPeriod(request, client);

    return movements.stream().map(MovementSearchResponse::fromModel).collect(Collectors.toList());
  }

  private MovementStrategy validateMomentTypeStrategy(MovementCmd movementCmd) {

    MovementType type = movementCmd.getType();

    if (MovementType.CREDIT.equals(type)) {
      return creditMovementStrategy;
    } else {
      return debitMovementStrategy;
    }
  }
}

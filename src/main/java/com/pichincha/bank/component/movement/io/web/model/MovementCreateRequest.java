package com.pichincha.bank.component.movement.io.web.model;

import com.pichincha.bank.component.movement.model.MovementType;
import com.pichincha.bank.component.movement.service.MovementService.MovementCmd;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class MovementCreateRequest implements Serializable {

  private static final long serialVersionUID = 1491390367447850683L;

  @NotNull
  private Date date;

  @NotNull
  @NotBlank
  private String type;

  @NotNull
  private double value;

  @NotNull
  private Long accountId;

  public static MovementCmd toModel(MovementCreateRequest request) {
    return MovementCmd.builder()
        .date(request.getDate())
        .type(MovementType.fromType(request.getType()))
        .value(request.getValue())
        .accountId(request.getAccountId())
        .build();
  }
}

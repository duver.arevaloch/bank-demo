package com.pichincha.bank.component.account.io.web;

import com.pichincha.bank.component.account.io.web.model.AccountCreateRequest;
import com.pichincha.bank.component.account.io.web.model.AccountCreateResponse;
import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.account.service.AccountService;
import com.pichincha.bank.component.account.service.AccountService.AccountCmd;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(path = "/api/v1/accounts", consumes = { MediaType.APPLICATION_JSON_VALUE },
    produces = { MediaType.APPLICATION_JSON_VALUE })
@Api(tags = { "Accounts" }, value = "Accounts")
@Slf4j
public class AccountController {

  @Autowired
  private AccountService accountService;

  @PostMapping
  @ApiOperation(value = "Create an account.")
  @ApiResponses(value = { @ApiResponse(code = 201, message = "Created.")
  })
  @ResponseStatus(value = HttpStatus.CREATED)
  public ResponseEntity<AccountCreateResponse> create(@Valid @NotNull @RequestBody AccountCreateRequest request)
      throws ResourceNotFoundException {
    log.debug("Begin create: request = {}", request);

    AccountCmd command = AccountCreateRequest.toModel(request);

    Account clientCreated = accountService.create(command);

    log.debug("End create: clientCreated = {}", clientCreated);

    return ResponseEntity.ok(AccountCreateResponse.fromModel(clientCreated));
  }
}

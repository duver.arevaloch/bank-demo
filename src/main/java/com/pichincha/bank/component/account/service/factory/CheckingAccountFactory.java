package com.pichincha.bank.component.account.service.factory;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.account.service.AccountService.AccountCmd;
import com.pichincha.bank.component.client.model.Client;
import lombok.NonNull;

public class CheckingAccountFactory implements AccountFactory {

  @Override
  public Account createAccount(@NonNull AccountCmd command, @NonNull Client client) {
    return AccountCmd.toModel(command, client);
  }
}

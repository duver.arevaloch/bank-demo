package com.pichincha.bank.component.account.service.factory;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.account.service.AccountService;
import com.pichincha.bank.component.client.model.Client;

public interface AccountFactory {

  Account createAccount(AccountService.AccountCmd accountCmd, Client client);
}

package com.pichincha.bank.component.account.io.repository;

import com.pichincha.bank.component.account.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  Optional<Account> findByNumber(String numberAccount);

  Optional<Account> findById(Long accountId);
}

package com.pichincha.bank.component.account.service;

import com.pichincha.bank.component.account.io.gateway.AccountGateway;
import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.account.model.AccountType;
import com.pichincha.bank.component.account.service.factory.AccountFactory;
import com.pichincha.bank.component.account.service.factory.CheckingAccountFactory;
import com.pichincha.bank.component.account.service.factory.SavingAccountFactory;
import com.pichincha.bank.component.client.io.gateway.ClientGateway;
import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.shared.web.exception.BadRequestException;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
public class AccountServiceImpl implements AccountService {

  private static final String NOT_SUPPORTED_ACCOUNT_EXCEPTION_MESSAGE = "The Account type is not currently supported.";

  private static final String CLIENT_NOT_FOUND_EXCEPTION_MESSAGE = "Client not found.";

  private static final String ACCOUNT_NUMBER_EXIST_EXCEPTION_MESSAGE = "Number Account exist.";

  @Autowired
  private AccountGateway accountGateway;

  @Autowired
  private ClientGateway clientGateway;

  @Transactional
  @Override
  public Account create(AccountCmd accountCmd) throws ResourceNotFoundException {
    log.debug("Begin create: accountCmd = {}", accountCmd);

    validateAccountNumber(accountCmd);

    Client client = clientGateway.findById(accountCmd.getClientId())
        .orElseThrow(() -> new ResourceNotFoundException(CLIENT_NOT_FOUND_EXCEPTION_MESSAGE));

    AccountFactory accountFactory = validateAccountType(accountCmd);

    Account accountToCreate = accountFactory.createAccount(accountCmd, client);

    Account accountCreated = accountGateway.create(accountToCreate);

    log.debug("End create: accountCreated = {}", accountCreated);

    return accountCreated;
  }

  private void validateAccountNumber(AccountCmd accountCmd) {
    if (accountGateway.findByNumber(accountCmd.getNumber()).isPresent()) {
      throw new BadRequestException(ACCOUNT_NUMBER_EXIST_EXCEPTION_MESSAGE);
    }
  }

  private AccountFactory validateAccountType(AccountCmd accountCmd) {
    AccountFactory accountFactory;
    AccountType type = accountCmd.getType();

    switch (type) {
    case SAVING:
      accountFactory = new SavingAccountFactory();
      break;
    case CHECKING:
      accountFactory = new CheckingAccountFactory();
      break;
    default:
      throw new BadRequestException(NOT_SUPPORTED_ACCOUNT_EXCEPTION_MESSAGE);
    }
    return accountFactory;
  }
}

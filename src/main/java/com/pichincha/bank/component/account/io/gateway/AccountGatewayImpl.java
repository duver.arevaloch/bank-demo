package com.pichincha.bank.component.account.io.gateway;

import com.pichincha.bank.component.account.io.repository.AccountRepository;
import com.pichincha.bank.component.account.model.Account;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public class AccountGatewayImpl implements AccountGateway {

  @Autowired
  private AccountRepository accountRepository;

  @Override
  public Account create(@NonNull Account account) {
    return accountRepository.save(account);
  }

  @Override
  public Optional<Account> findByNumber(String numberAccount) {
    return accountRepository.findByNumber(numberAccount);
  }

  @Override
  public Optional<Account> findById(Long accountId) {
    return accountRepository.findById(accountId);
  }
}

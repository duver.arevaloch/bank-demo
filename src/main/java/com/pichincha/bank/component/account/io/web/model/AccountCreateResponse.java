package com.pichincha.bank.component.account.io.web.model;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.account.model.AccountType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import java.io.Serializable;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountCreateResponse implements Serializable {

  private static final long serialVersionUID = -5610504575919138935L;

  private String number;

  private AccountType type;

  private double balanceInitial;

  private boolean state;

  private String clientName;

  public static AccountCreateResponse fromModel(Account account) {
    return AccountCreateResponse.builder()
        .number(account.getNumber())
        .type(account.getType())
        .balanceInitial(account.getBalanceInitial())
        .state(account.isState())
        .clientName(account.getClient().getName())
        .build();
  }
}

package com.pichincha.bank.component.account.io.gateway;

import com.pichincha.bank.component.account.model.Account;
import lombok.NonNull;
import java.util.Optional;

public interface AccountGateway {

  Account create(@NonNull Account account);

  Optional<Account> findByNumber(String numberAccount);

  Optional<Account> findById(Long accountId);

}

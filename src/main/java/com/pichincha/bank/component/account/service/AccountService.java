package com.pichincha.bank.component.account.service;

import com.pichincha.bank.component.account.model.Account;
import com.pichincha.bank.component.account.model.AccountType;
import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

public interface AccountService {

  Account create(AccountCmd accountCmd) throws ResourceNotFoundException;

  @Data
  @Generated
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  class AccountCmd {
    private String number;

    private AccountType type;

    private double balanceInitial;

    private boolean state;

    private Long clientId;

    public static Account toModel(AccountCmd command, Client client) {
      return Account.builder()
          .number(command.getNumber())
          .type(command.getType())
          .balanceInitial(command.getBalanceInitial())
          .state(command.isState())
          .client(client)
          .build();
    }
  }
}

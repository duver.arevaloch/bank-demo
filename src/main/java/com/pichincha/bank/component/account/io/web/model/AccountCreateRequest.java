package com.pichincha.bank.component.account.io.web.model;

import com.pichincha.bank.component.account.model.AccountType;
import com.pichincha.bank.component.account.service.AccountService.AccountCmd;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AccountCreateRequest implements Serializable {

  private static final long serialVersionUID = 1491390567447850683L;

  @NotNull
  @NotBlank
  private String number;

  @NotNull
  @NotBlank
  private String type;

  @NotNull
  private double balanceInitial;

  private boolean state = Boolean.TRUE;

  @NotNull
  private Long clientId;

  public static AccountCmd toModel(AccountCreateRequest request) {
    return AccountCmd.builder()
        .number(request.getNumber())
        .type(AccountType.fromType(request.getType()))
        .balanceInitial(request.getBalanceInitial())
        .state(request.isState())
        .clientId(request.getClientId())
        .build();
  }
}

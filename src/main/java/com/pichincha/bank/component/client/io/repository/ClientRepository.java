package com.pichincha.bank.component.client.io.repository;

import com.pichincha.bank.component.client.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface ClientRepository extends JpaRepository<Client, Long> {

  Optional<Client> findById(Long id);
}

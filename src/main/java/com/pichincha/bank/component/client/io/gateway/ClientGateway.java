package com.pichincha.bank.component.client.io.gateway;

import com.pichincha.bank.component.client.model.Client;
import lombok.NonNull;
import java.util.Optional;

public interface ClientGateway {
  Client create(@NonNull Client client);

  Optional<Client> findById(Long clientId);

  Client update(@NonNull Client client);

  void delete(@NonNull Client client);
}

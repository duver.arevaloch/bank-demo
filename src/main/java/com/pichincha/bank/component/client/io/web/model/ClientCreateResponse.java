package com.pichincha.bank.component.client.io.web.model;

import com.pichincha.bank.component.client.model.Client;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientCreateResponse {

  private String name;

  private String address;

  private String phone;

  private boolean state;

  public static ClientCreateResponse fromModel(Client client) {
    return ClientCreateResponse.builder()
        .name(client.getName())
        .address(client.getAddress())
        .phone(client.getPhone())
        .state(client.isState())
        .build();
  }
}

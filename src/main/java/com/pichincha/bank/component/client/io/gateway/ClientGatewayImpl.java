package com.pichincha.bank.component.client.io.gateway;

import com.pichincha.bank.component.client.io.repository.ClientRepository;
import com.pichincha.bank.component.client.model.Client;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public class ClientGatewayImpl implements ClientGateway {

  @Autowired
  private ClientRepository clientRepository;

  @Override
  public Client create(@NonNull Client client) {
    return clientRepository.save(client);
  }

  @Override
  public Optional<Client> findById(Long clientId) {
    return clientRepository.findById(clientId);
  }

  @Override
  public Client update(@NonNull Client client) {
    return clientRepository.save(client);
  }

  @Override
  public void delete(@NonNull Client client) {
    clientRepository.delete(client);
  }
}

package com.pichincha.bank.component.client.io.web;

import com.pichincha.bank.component.client.io.web.model.ClientCreateRequest;
import com.pichincha.bank.component.client.io.web.model.ClientCreateResponse;
import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.client.service.ClientService;
import com.pichincha.bank.component.client.service.ClientService.ClientCmd;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(path = "/api/v1/clients", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@Api(tags = { "Clients" }, value = "Clients")
@Slf4j
public class ClientController {

  @Autowired
  private ClientService clientService;

  @PostMapping
  @ApiOperation(value = "Create a client.")
  @ApiResponses(value = { @ApiResponse(code = 201, message = "Created.")
  })
  @ResponseStatus(value = HttpStatus.CREATED)
  public ResponseEntity<ClientCreateResponse> create(@Valid @NotNull @RequestBody ClientCreateRequest request) {
    log.debug("Begin create: request = {}", request);

    ClientCmd command = ClientCreateRequest.toModel(request);

    Client clientCreated = clientService.create(command);

    log.debug("End create: clientCreated = {}", clientCreated);

    return ResponseEntity.status(HttpStatus.CREATED).body(ClientCreateResponse.fromModel(clientCreated));
  }

  @PutMapping(path = "/{id}")
  @ApiOperation(value = "Update a client.")
  @ApiResponses(value = { @ApiResponse(code = 200, message = "Success.")
  })
  public ResponseEntity<ClientCreateResponse> update(@Valid @NotNull @RequestBody ClientCreateRequest request,
      @Valid @PathVariable("id") @NotNull Long id) throws ResourceNotFoundException {

    log.debug("Begin update: request = {}", request);

    ClientCmd command = ClientCreateRequest.toModel(request);

    Client clientUpdated = clientService.update(id, command);

    log.debug("End update: clientCreated = {}", clientUpdated);

    return ResponseEntity.ok(ClientCreateResponse.fromModel(clientUpdated));
  }

  @DeleteMapping(path = "/{id}")
  @ApiOperation(value = "Delete a client.")
  @ApiResponses(value = { @ApiResponse(code = 204, message = "Success.")
  })
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public ResponseEntity<Void> delete(@Valid @PathVariable("id") @NotNull Long id) throws ResourceNotFoundException {

    log.debug("Begin delete: id = {}", id);

    clientService.delete(id);

    return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
  }
}

package com.pichincha.bank.component.client.service;

import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;
import lombok.NonNull;

public interface ClientService {

  Client create(@NonNull ClientCmd command);

  Client update(@NonNull Long id, @NonNull ClientCmd command) throws ResourceNotFoundException;

  void  delete(@NonNull Long clientId) throws ResourceNotFoundException;

  @Data
  @Generated
  @NoArgsConstructor
  @AllArgsConstructor
  @Builder
  class ClientCmd {

    private String name;

    private String address;

    private String phone;

    private String password;

    private String identification;

    private String gender;

    private boolean state;

    public static Client toModel(ClientCmd command) {
      Client client = new Client();
      client.setName(command.getName());
      client.setPassword(command.getPassword());
      client.setAddress(command.getAddress());
      client.setPhone(command.getPhone());
      client.setState(command.isState());
      client.setGender(command.getGender());
      client.setIdentification(command.getIdentification());
      return client;
    }

    public static Client toModel(Client client , ClientCmd command) {
      client.setName(command.getName());
      client.setPassword(command.getPassword());
      client.setAddress(command.getAddress());
      client.setPhone(command.getPhone());
      client.setState(command.isState());
      client.setGender(command.getGender());
      client.setIdentification(command.getIdentification());
      return client;
    }
  }
}

package com.pichincha.bank.component.client.service;

import com.pichincha.bank.component.client.io.gateway.ClientGateway;
import com.pichincha.bank.component.client.model.Client;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.validation.constraints.NotNull;

@Service
@Slf4j
public class ClientServiceImpl implements ClientService {

  private static final String CLIENT_NOT_FOUND_EXCEPTION_MESSAGE = "Client not found.";

  @Autowired
  private ClientGateway clientGateway;

  @Override
  public Client create(@NonNull ClientCmd command) {
    log.debug("Begin create command = {}", command);

    Client clientToCreate = ClientCmd.toModel(command);
    Client clientCreated =  clientGateway.create(clientToCreate);

    log.debug("End create clientCreated = {}", clientCreated);

    return clientCreated;
  }

  @Override
  public Client update(@NotNull Long id, @NonNull ClientCmd command) throws ResourceNotFoundException {
    log.debug("Begin update command = {}", command);

    Client client = clientGateway.findById(id)
        .orElseThrow(() -> new ResourceNotFoundException(CLIENT_NOT_FOUND_EXCEPTION_MESSAGE));

    Client clientToUpdate = ClientCmd.toModel(client, command);
    Client clientUpdated =  clientGateway.update(clientToUpdate);

    log.debug("End update command = {}", command);

    return clientUpdated;
  }

  @Override
  public void delete(@NonNull Long clientId) throws ResourceNotFoundException {
    log.debug("Begin update clientId = {}", clientId);

    Client client = clientGateway.findById(clientId)
        .orElseThrow(() -> new ResourceNotFoundException(CLIENT_NOT_FOUND_EXCEPTION_MESSAGE));

    clientGateway.delete(client);
  }

}

package com.pichincha.bank.component.client.io.web.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import com.pichincha.bank.component.client.service.ClientService.ClientCmd;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClientCreateRequest {

  @NotNull
  @NotBlank
  private String name;

  @NotNull
  @NotBlank
  private String address;

  @NotNull
  @NotBlank
  private String phone;

  @NotNull
  @NotBlank
  private String password;

  @NotNull
  @NotBlank
  private String identification;

  private boolean state =  Boolean.TRUE;

  public static ClientCmd toModel(ClientCreateRequest request) {
    return ClientCmd.builder()
        .name(request.getName())
        .address(request.getAddress())
        .phone(request.getPhone())
        .password(request.getPassword())
        .state(request.isState())
        .identification(request.getIdentification())
        .build();
  }
}

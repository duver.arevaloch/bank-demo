package com.pichincha.bank.component.shared.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends Exception {

  private static final long serialVersionUID = 7807486545061677473L;

  public ResourceNotFoundException(final String message) {
    super(message);
  }
}

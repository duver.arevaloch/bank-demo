package com.pichincha.bank.component.shared.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

  private static final long serialVersionUID = 7584939375464084747L;

  public BadRequestException(final String message) {
    super(message);
  }

}

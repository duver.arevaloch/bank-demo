package com.pichincha.bank.component.shared.web.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Data
@Generated
@NoArgsConstructor
@AllArgsConstructor
public class CustomErrorResponse {

  private int    status;
  private String message;
}

package com.pichincha.bank.config;

import com.pichincha.bank.component.shared.web.exception.BadRequestException;
import com.pichincha.bank.component.shared.web.exception.CustomErrorResponse;
import com.pichincha.bank.component.shared.web.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class CustomizedResponseEntityExceptionHandler {

  @ExceptionHandler
  public ResponseEntity<?> handleException(BadRequestException ex) {
    return createErrorResponse(HttpStatus.BAD_REQUEST, ex.getMessage());
  }

  @ExceptionHandler
  public ResponseEntity<?> handleException(ResourceNotFoundException ex) {
    return createErrorResponse(HttpStatus.NOT_FOUND, ex.getMessage());
  }
  private ResponseEntity<CustomErrorResponse> createErrorResponse(HttpStatus status, String errorMessage) {
    return createErrorResponse(status, errorMessage, null);
  }

  private ResponseEntity<CustomErrorResponse> createErrorResponse(HttpStatus status, String errorMessage,
      Exception ex) {
    if (ex != null) {
      log.error(ex.getMessage(), ex);
    }

    CustomErrorResponse error = new CustomErrorResponse();
    error.setMessage(errorMessage);
    error.setStatus(status.value());
    return new ResponseEntity<>(error, status);
  }
}

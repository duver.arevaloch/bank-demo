FROM openjdk:11-jre-slim

WORKDIR /app

COPY target/bank-demo-0.0.1-SNAPSHOT.jar /app/bank-demo.jar

CMD ["java", "-jar", "/app/bank-demo.jar"]